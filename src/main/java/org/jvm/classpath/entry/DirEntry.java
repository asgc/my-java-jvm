package org.jvm.classpath.entry;


import org.jvm.util.JvmUtil;

import java.io.*;

public class DirEntry implements Entry {

    /**
     * 类目录
     */
    private String absDir;

    public DirEntry(String path) {
        this.absDir = path.endsWith(File.separator) ? path : path.concat(File.separator);
    }

    public String getAbsDir() {
        return absDir;
    }

    public void setAbsDir(String absDir) {
        this.absDir = absDir;
    }

    /**
     * classname中分隔符是"/"
     * windows系统分隔符是"\"
     * 在构建FileInputStream时可以混用这两种分隔符
     *
     * @param className
     * @return
     */
    public byte[] readClass(String className) {
        String filePath = absDir
                .concat(className);
        InputStream is = null;
        try {
            is = new FileInputStream(filePath);
            return JvmUtil.readInputStream(is);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath() {
        return getAbsDir();
    }
}
