package org.jvm.rtda.thread;

import org.jvm.rtda.Object;
import org.jvm.rtda.*;

/**
 * 栈帧中的本地变量表
 *
 * @author 海燕
 * @date 2023/1/12
 */
public class LocalVars extends Slots {

    /**
     * @param maxLocals 最大本地变量表尺寸
     */
    public LocalVars(int maxLocals) {
        super(maxLocals);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("LocalVars:[");
        for (int i = 0; i < this.localVars.length; i++) {
            stringBuilder.append(this.localVars[i].getNum() + ",");
        }
        return stringBuilder.append("]").toString();
    }

    public Object getThis() {
        return getRef(0);
    }
}
