package org.jvm.rtda.thread;

/**
 * 线程所持有的方法栈.栈中持有栈帧
 *
 * @author 海燕
 * @date 2023/1/12
 */
public class Stack {

    /**
     * 方法栈最大深度，如果超过会抛出StackOverFlow
     */
    private final int maxSize;
    /**
     * 栈帧
     */
    private final java.util.Stack<Frame> stack;


    public Stack(int maxSize) {
        this.maxSize = maxSize;
        this.stack = new java.util.Stack<>();
    }


    public void push(Frame frame) {
        //栈溢出
        if (stack.size() >= maxSize) {
            throw new RuntimeException("StackOverFlow");
        }
        stack.push(frame);
    }

    public Frame pop() {
        return stack.pop();
    }


    public Frame top() {
        if (stack.empty()) {
            return null;
        }
        return stack.peek();
    }

    public boolean empty() {
        return this.stack.empty();
    }

    public void clear() {
        for (; !empty(); pop()) ;
    }

    public java.util.Stack<Frame> getStack() {
        return stack;
    }
}
