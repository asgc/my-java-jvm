package org.jvm.rtda;

/**
 * 用于本地变量表和操作数栈中存储变量的对象
 * 支持32位基本类型量（long或double使用两个slot表示），引用类型量
 *
 * @author 海燕
 * @date 2023/1/12
 */
public class Slot implements Cloneable {
    /**
     * 基本类型
     */
    private int num;
    /**
     * 引用类型
     */
    private Object ref;


    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Object getRef() {
        return ref;
    }

    public void setRef(Object ref) {
        this.ref = ref;
    }

    /**
     * 返回自身的一个克隆
     *
     * @return
     */
    public Slot clone() {
        Slot clone = new Slot();
        clone.setRef(this.getRef());
        clone.setNum(this.getNum());
        return clone;
    }
}
