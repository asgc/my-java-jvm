package org.jvm.rtda.heap.classmember;

import org.jvm.rtda.heap.symref.ClassRef;

/**
 * 异常处理信息
 *
 * @author 海燕
 * @date 2023/2/23
 */
public class ExceptionHandler {

    /**
     * try块字节码起始位置
     */
    private int startPc;

    /**
     * try块字节码结束位置
     */
    private int endPc;

    /**
     * catch块字节码起始位置
     */
    private int handlerPc;

    /**
     * 支持处理的异常类型
     */
    private ClassRef catchType;

    public ExceptionHandler(int startPc, int endPc, int handlerPc, ClassRef catchType) {
        this.startPc = startPc;
        this.endPc = endPc;
        this.handlerPc = handlerPc;
        this.catchType = catchType;
    }

    public int getStartPc() {
        return startPc;
    }

    public int getEndPc() {
        return endPc;
    }

    public int getHandlerPc() {
        return handlerPc;
    }

    public ClassRef getCatchType() {
        return catchType;
    }
}
