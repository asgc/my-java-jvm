package org.jvm.rtda.heap.symref;

import org.jvm.rtda.heap.ConstantPool;

/**
 * 类成员引用
 *
 * @author 王思翔
 * @date 2023/2/4
 */
public class MemberRef extends SymRef {

    /**
     * 类成员名称
     */
    protected String name;

    /**
     * 类成员描述
     */
    protected String descriptor;

    public MemberRef(ConstantPool cp, String className, String name, String descriptor) {
        super(cp, className);
        this.name = name;
        this.descriptor = descriptor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }
}
