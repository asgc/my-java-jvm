package org.jvm.rtda.heap.symref;

import org.jvm.rtda.heap.*;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.thread.Thread;

/**
 * 非接口方法引用,指通过类或抽象类调用的方法
 *
 * @author 王思翔
 * @date 2023/2/4
 */
public class MethodRef extends MemberRef {

    private Method method;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public MethodRef(ConstantPool cp, String className, String name, String descriptor) {
        super(cp, className, name, descriptor);
    }

    /**
     * 解析方法引用
     *
     * @return
     */
    public Method resolveMethodRef(Thread thread) {
        //检查类是否是接口，非接口方法引用的引用类不能为接口
        Klass thatClass = this.resolvedClass(thread);
        if (thatClass.isInterface()) {
            throw new RuntimeException("class is interface");
        }
        //查找类
        Method method = MethodUtil.lookupMethod(thatClass, this.name, this.descriptor);
        if (method == null) {
            throw new RuntimeException("method no found");
        }
        //检查权限
        if (!method.isAccessibleTo(this.cp.getKlass())) {
            throw new RuntimeException("method can not accessible");
        }
        this.method = method;
        return method;
    }

}
