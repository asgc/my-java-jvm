package org.jvm.nativemethod.methods.sun.misc;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class URLClassPathNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("sun/misc/URLClassPath", "getLookupCacheURLs", "(Ljava/lang/ClassLoader;)[Ljava/net/URL;", URLClassPathNativeMethod.class.getDeclaredMethod("getLookupCacheURLs", Frame.class));

    }


    public static void getLookupCacheURLs(Frame frame) {
        frame.getOperandStack().pushRef(null);
    }

}
