package org.jvm.nativemethod.methods.sun.io;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/19
 */
public class Win32ErrorModeNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("sun/io/Win32ErrorMode", "setErrorMode", "(J)J", Win32ErrorModeNativeMethod.class.getDeclaredMethod("setErrorMode", Frame.class));
    }

    public static void setErrorMode(Frame frame) {
        frame.getOperandStack().pushLong(0);
    }

}
