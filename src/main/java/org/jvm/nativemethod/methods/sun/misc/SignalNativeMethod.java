package org.jvm.nativemethod.methods.sun.misc;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.*;

/**
 * @author 海燕
 * @date 2023/2/19
 */
public class SignalNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("sun/misc/Signal", "findSignal", "(Ljava/lang/String;)I", SignalNativeMethod.class.getDeclaredMethod("findSignal", Frame.class));
        register("sun/misc/Signal", "handle0", "(IJ)J", SignalNativeMethod.class.getDeclaredMethod("handle0", Frame.class));
    }

    public static void findSignal(Frame frame) {
        LocalVars vars = frame.getLocalVars();
        Object ref = vars.getRef(0);

        frame.getOperandStack().pushInt(0);
    }

    public static void handle0(Frame frame) {
        LocalVars vars = frame.getLocalVars();
        vars.getInt(0);
        vars.getLong(1);

        frame.getOperandStack().pushLong(0);
    }

}
