package org.jvm.nativemethod.methods.java.lang;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class FloatNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/lang/Float", "floatToRawIntBits", "(F)I", FloatNativeMethod.class.getDeclaredMethod("floatToRawIntBits", Frame.class));
        register("java/lang/Float", "intBitsToFloat", "(I)F", FloatNativeMethod.class.getDeclaredMethod("intBitsToFloat", Frame.class));

    }

    public static void floatToRawIntBits(Frame frame) {
        float value = frame.getLocalVars().getFloat(0);
        frame.getOperandStack().pushInt(Float.floatToRawIntBits(value));
    }


    public static void intBitsToFloat(Frame frame) {
        int value = frame.getLocalVars().getInt(0);
        frame.getOperandStack().pushFloat(Float.intBitsToFloat(value));
    }

}
