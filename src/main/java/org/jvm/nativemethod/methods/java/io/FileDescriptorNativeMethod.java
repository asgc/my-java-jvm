package org.jvm.nativemethod.methods.java.io;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class FileDescriptorNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/io/FileDescriptor", "set", "(I)J", FileDescriptorNativeMethod.class.getDeclaredMethod("set", Frame.class));
    }

    public static void set(Frame frame) {
        frame.getOperandStack().pushLong(0);
    }
}
