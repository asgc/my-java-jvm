package org.jvm.nativemethod.methods.java.lang;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class RuntimeNativeMethod extends NativeMethodRegister {
    private static String jlRuntime = "java/lang/Runtime";

    public static void init() throws NoSuchMethodException {
        register(jlRuntime, "availableProcessors", "()I", RuntimeNativeMethod.class.getDeclaredMethod("availableProcessors", Frame.class));
    }

    public static void availableProcessors(Frame frame) {
        int numCPU = Runtime.getRuntime().availableProcessors();
        frame.getOperandStack().pushInt(numCPU);
    }

}
