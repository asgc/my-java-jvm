package org.jvm.nativemethod.methods.java.io;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.*;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class FileOutputStreamNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/io/FileOutputStream", "writeBytes", "([BIIZ)V", FileOutputStreamNativeMethod.class.getDeclaredMethod("writeBytes", Frame.class));

    }

    public static void writeBytes(Frame frame) {
        LocalVars vars = frame.getLocalVars();
        //this := vars.GetRef(0)
        Object b = vars.getRef(1);
        int off = vars.getInt(2);
        int len = vars.getInt(3);
        //append := vars.GetBoolean(4)
        byte[] jBytes = b.bytes();
        char[] chars = new char[len];
        for (int i = off; i < off + len; i++) {
            chars[i - off] = (char) jBytes[i];
        }
        System.out.print(new String(chars));
    }

}
