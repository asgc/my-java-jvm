package org.jvm.nativemethod.methods;

import org.jvm.rtda.thread.Frame;

/**
 * 本地方法
 *
 * @author 海燕
 * @date 2023/2/19
 */
public class NativeMethod {

    /**
     * 方法体为空的本地方法，不对栈帧做任何操作
     *
     * @param frame
     */
    public static void emptyNativeMethod(Frame frame) {

    }


}
