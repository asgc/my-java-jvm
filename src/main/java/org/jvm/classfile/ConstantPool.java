package org.jvm.classfile;

import org.jvm.classfile.constantinfo.*;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :.class文件常量池，持有多个常量信息
 */
public class ConstantPool {

    private final ConstantInfo[] constantInfos;

    public ConstantPool(ClassReader classReader) {
        this.constantInfos = ConstantInfoBuilder.readConstantInfos(classReader, this);
    }

    public ConstantInfo[] getConstantInfos() {
        return constantInfos;
    }

    public ConstantInfo getConstantInfoByIndex(int index) {
        return this.constantInfos[index];
    }

    public String getUtf8(int index) {
        ConstantUtf8Info constantUtf8Info = (ConstantUtf8Info) this.constantInfos[index];
        return constantUtf8Info.getVal();
    }

}
