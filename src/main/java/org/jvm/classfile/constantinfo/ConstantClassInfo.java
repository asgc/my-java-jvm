package org.jvm.classfile.constantinfo;

import org.jvm.classfile.*;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantClassInfo implements ConstantInfo {

    private final ConstantPool constantPool;

    /**
     * 指向常量池的一个Utf-8字符组常量的索引，表示类的全类名
     */
    private int nameIndex;

    public ConstantClassInfo(ConstantPool constantPool) {
        this.constantPool = constantPool;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.nameIndex = classReader.readUint16();
    }

    /**
     * 根据索引寻址，返回对应字符串
     *
     * @return
     */
    public String name() {
        return constantPool.getUtf8(this.nameIndex);
    }
}
