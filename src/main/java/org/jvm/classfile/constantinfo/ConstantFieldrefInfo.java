package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ConstantPool;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :本类中引用的其他类字段
 */
public class ConstantFieldrefInfo extends ConstantMemberrefInfo {

    public ConstantFieldrefInfo(ConstantPool constantPool) {
        super(constantPool);
    }
}
