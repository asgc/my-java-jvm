package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ConstantPool;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :本类中引用的其他类接口方法
 */
public class ConstantInterfaceMethodrefInfo extends ConstantMemberrefInfo {


    public ConstantInterfaceMethodrefInfo(ConstantPool constantPool) {
        super(constantPool);
    }
}
