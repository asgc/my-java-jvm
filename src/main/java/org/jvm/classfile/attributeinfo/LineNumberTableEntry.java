package org.jvm.classfile.attributeinfo;

/**
 * 行号表
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class LineNumberTableEntry {

    private final int startPc;

    private final int lineNumber;


    public LineNumberTableEntry(int startPc, int lineNumber) {
        this.startPc = startPc;
        this.lineNumber = lineNumber;
    }

    public int getStartPc() {
        return startPc;
    }

    public int getLineNumber() {
        return lineNumber;
    }
}
