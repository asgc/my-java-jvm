package org.jvm.classfile.attributeinfo;

import org.jvm.classfile.ClassReader;

/**
 * 标识基本类型常量
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class ConstantValueAttribute implements AttributeInfo {

    /**
     * 指向常量池中基本类型常量的索引
     */
    private int constantValueIndex;

    /**
     * @param classReader
     * @param attributeLength 作为一个定长属性，属性长度一定是2
     */
    @Override
    public void readInfo(ClassReader classReader, int attributeLength) {
        this.constantValueIndex = classReader.readUint16();
    }

    public int getConstantValueIndex() {
        return constantValueIndex;
    }
}
