package org.jvm.classfile.attributeinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :.class文件的属性信息
 */
public interface AttributeInfo {

    void readInfo(ClassReader classReader, int attributeLength);

}
