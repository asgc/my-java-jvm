package org.jvm.classfile.attributeinfo;

/**
 * 异常处理表
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class ExceptionTableEntry {

    private final int startPc;

    private final int endPc;

    private final int handlerPc;

    /**
     * 指向类常量池中一个类引用符号的索引
     */
    private final int catchType;

    public ExceptionTableEntry(int startPc, int endPc, int handlerPc, int catchType) {
        this.startPc = startPc;
        this.endPc = endPc;
        this.handlerPc = handlerPc;
        this.catchType = catchType;
    }

    public int getStartPc() {
        return startPc;
    }

    public int getEndPc() {
        return endPc;
    }

    public int getHandlerPc() {
        return handlerPc;
    }

    public int getCatchType() {
        return catchType;
    }
}
