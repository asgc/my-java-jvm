package org.jvm.classfile.attributeinfo;

/**
 * 用以标识源文件中不存在，由编译器生成的类成员
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class SyntheticAttribute extends MarkerAttribute {

}
