package org.jvm.instruction.conversions.l2x;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 类型转换指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class L2D extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        long val = frame.getOperandStack().popLong();
        frame.getOperandStack().pushDouble((double) val);
    }
}
