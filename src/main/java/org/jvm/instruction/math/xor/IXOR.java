package org.jvm.instruction.math.xor;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 求亦或
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class IXOR extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        int v1 = frame.getOperandStack().popInt();
        int v2 = frame.getOperandStack().popInt();
        frame.getOperandStack().pushInt(v1 ^ v2);
    }
}
