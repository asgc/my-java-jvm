package org.jvm.instruction.math.sub;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * double相减
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class DSUB extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        double v2 = frame.getOperandStack().popDouble();
        double v1 = frame.getOperandStack().popDouble();
        frame.getOperandStack().pushDouble(v1 - v2);
    }
}
