package org.jvm.instruction.math.neg;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 求反
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class INEG extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        int val = frame.getOperandStack().popInt();
        frame.getOperandStack().pushInt(-val);
    }
}
