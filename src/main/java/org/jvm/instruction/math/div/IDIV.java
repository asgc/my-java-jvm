package org.jvm.instruction.math.div;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 除法指令
 *
 * @author 海燕
 * @date 2023/1/17
 */
public class IDIV extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //被除数dividend  除数divisor
        int divisor = frame.getOperandStack().popInt();
        int dividend = frame.getOperandStack().popInt();
        frame.getOperandStack().pushInt(dividend / divisor);
    }
}
