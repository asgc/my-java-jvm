package org.jvm.instruction.math.div;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * double除法指令
 *
 * @author 海燕
 * @date 2023/1/17
 */
public class DDIV extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //被除数dividend  除数divisor
        double divisor = frame.getOperandStack().popDouble();
        double dividend = frame.getOperandStack().popDouble();
        frame.getOperandStack().pushDouble(dividend / divisor);
    }
}
