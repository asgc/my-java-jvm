package org.jvm.instruction.math.mul;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 乘法指令
 *
 * @author 海燕
 * @date 2023/1/17
 */
public class DMUL extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        double v1 = frame.getOperandStack().popDouble();
        double v2 = frame.getOperandStack().popDouble();
        frame.getOperandStack().pushDouble(v2 * v1);
    }
}
