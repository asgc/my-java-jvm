package org.jvm.instruction.math.mul;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 乘法指令
 *
 * @author 海燕
 * @date 2023/1/17
 */
public class IMUL extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        int v1 = frame.getOperandStack().popInt();
        int v2 = frame.getOperandStack().popInt();
        frame.getOperandStack().pushInt(v2 * v1);
    }
}
