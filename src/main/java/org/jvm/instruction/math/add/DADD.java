package org.jvm.instruction.math.add;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * double相加指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class DADD extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        double v1 = frame.getOperandStack().popDouble();
        double v2 = frame.getOperandStack().popDouble();
        frame.getOperandStack().pushDouble(v1 + v2);
    }
}
