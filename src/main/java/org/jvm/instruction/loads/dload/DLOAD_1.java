package org.jvm.instruction.loads.dload;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 根据读取到的操作数，从局部变量表对应索引处读出double变量，压入操作数栈
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class DLOAD_1 extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushDouble(frame.getLocalVars().getDouble(1));
    }
}
