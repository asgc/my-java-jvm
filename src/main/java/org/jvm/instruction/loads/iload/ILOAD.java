package org.jvm.instruction.loads.iload;

import org.jvm.instruction.base.Index8Instruction;
import org.jvm.rtda.thread.Frame;

/**
 * 根据读取到的操作数，从局部变量表对应索引处读出int变量，压入操作数栈
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class ILOAD extends Index8Instruction {
    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushInt(frame.getLocalVars().getInt(this.index));
    }
}
