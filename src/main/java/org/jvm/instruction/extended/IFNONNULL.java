package org.jvm.instruction.extended;

import org.jvm.instruction.base.BRANCH;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public class IFNONNULL extends BRANCH {
    @Override
    protected boolean getCond(Frame frame) {
        return frame.getOperandStack().popRef() != null;
    }
}
