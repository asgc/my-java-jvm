package org.jvm.instruction.stores.fstore;

import org.jvm.instruction.base.Index8Instruction;
import org.jvm.rtda.thread.Frame;

/**
 * 存储指令，将操作数栈弹栈并存入本地变量表
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class FSTORE extends Index8Instruction {
    @Override
    public void execute(Frame frame) {
        frame.getLocalVars().setFloat(this.index, frame.getOperandStack().popFloat());
    }
}
