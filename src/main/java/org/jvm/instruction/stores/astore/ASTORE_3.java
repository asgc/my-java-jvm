package org.jvm.instruction.stores.astore;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 存储指令，将操作数栈弹栈并存入本地变量表
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class ASTORE_3 extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        frame.getLocalVars().setRef(3, frame.getOperandStack().popRef());
    }
}
