package org.jvm.instruction.constants;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * nop指令不做任何操作
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class NOP extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {

    }
}
