package org.jvm.instruction.references.array.aload;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.*;

/**
 * @author 海燕
 * @date 2023/2/15
 */
public class CALOAD extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        OperandStack operandStack = frame.getOperandStack();
        int index = operandStack.popInt();
        Object arrayObject = operandStack.popRef();
        if (arrayObject == null) {
            frame.getThread().throwNullPointerException();
            return;
        }
        if (index < 0 || index >= arrayObject.arrayLength()) {
            frame.getThread().throwArrayIndexOutOfBoundsException();
            return;
        }
        char c = arrayObject.chars()[index];
        operandStack.pushInt(c & 0x0FFFF);
    }
}
