package org.jvm.instruction.references.array.astore;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.*;

/**
 * @author 海燕
 * @date 2023/2/15
 */
public class LASTORE extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        OperandStack operandStack = frame.getOperandStack();
        long val = operandStack.popLong();
        int index = operandStack.popInt();
        Object arrayObject = operandStack.popRef();
        if (arrayObject == null) {
            frame.getThread().throwNullPointerException();
            return;
        }
        if (index < 0 || index >= arrayObject.arrayLength()) {
            frame.getThread().throwArrayIndexOutOfBoundsException();
            return;
        }

        arrayObject.longs()[index] = val;
    }
}
