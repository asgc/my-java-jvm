package org.jvm.instruction.references;

import org.jvm.instruction.base.Index16Instruction;
import org.jvm.rtda.thread.Frame;

/**
 * 从常量池中获取常量值，压操作数栈
 *
 * @author 海燕
 * @date 2023/2/7
 */
public class LDC_W extends Index16Instruction {

    @Override
    public void execute(Frame frame) {
        LDC.exe(frame, this.index);
    }
}
