package org.jvm.instruction.references.invokemethod;

import org.jvm.instruction.base.*;
import org.jvm.rtda.heap.*;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.heap.symref.MethodRef;
import org.jvm.rtda.thread.Frame;

/**
 * 调用静态方法指令
 *
 * @author 海燕
 * @date 2023/2/11
 */
public class INVOKE_STATIC extends Index16Instruction {
    @Override
    public void execute(Frame frame) {
        ConstantPool constantPool = frame.getMethod().getKlass().getConstantPool();
        MethodRef methodRef = (MethodRef) constantPool.getConstant(this.index);
        Method method = methodRef.resolveMethodRef(frame.getThread());
        if (!method.isStatic()) {
            throw new RuntimeException("method is not static");
        }
        //执行类初始化
        Klass methodKlass = method.getKlass();
        InstructionUtil.initClass(frame.getThread(), methodKlass);
        InstructionUtil.invokeMethod(frame, method);
    }
}
