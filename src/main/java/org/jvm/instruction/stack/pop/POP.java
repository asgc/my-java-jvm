package org.jvm.instruction.stack.pop;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 从操作数栈中弹出单个slot
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class POP extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().popSlot();
    }
}
