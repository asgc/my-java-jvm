package org.jvm.instruction.stack.swap;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Slot;
import org.jvm.rtda.thread.Frame;

/**
 * 交换栈顶两个元素
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class SWAP extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        Slot frist = frame.getOperandStack().popSlot();
        Slot second = frame.getOperandStack().popSlot();
        frame.getOperandStack().pushSlot(frist);
        frame.getOperandStack().pushSlot(second);
    }
}
