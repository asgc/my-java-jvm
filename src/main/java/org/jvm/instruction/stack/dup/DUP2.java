package org.jvm.instruction.stack.dup;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Slot;
import org.jvm.rtda.thread.Frame;

/**
 * 复制栈顶两个元素
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class DUP2 extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        Slot frist = frame.getOperandStack().popSlot();
        Slot second = frame.getOperandStack().popSlot();
        frame.getOperandStack().pushSlot(second.clone());
        frame.getOperandStack().pushSlot(frist.clone());
        frame.getOperandStack().pushSlot(second);
        frame.getOperandStack().pushSlot(frist);
    }
}
