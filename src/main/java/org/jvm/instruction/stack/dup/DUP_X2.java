package org.jvm.instruction.stack.dup;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Slot;
import org.jvm.rtda.thread.Frame;

/**
 * 复制栈顶元素，并将栈顶元素下移3位
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class DUP_X2 extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        Slot first = frame.getOperandStack().popSlot();
        Slot second = frame.getOperandStack().popSlot();
        Slot third = frame.getOperandStack().popSlot();

        frame.getOperandStack().pushSlot(first.clone());
        frame.getOperandStack().pushSlot(third);
        frame.getOperandStack().pushSlot(second);
        frame.getOperandStack().pushSlot(first);

    }
}
