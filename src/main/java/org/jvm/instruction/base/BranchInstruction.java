package org.jvm.instruction.base;

/**
 * 用以表示跳转指令
 *
 * @author 海燕
 * @date 2023/1/14
 */
public abstract class BranchInstruction implements Instruction {

    /**
     * 程序计数器偏移量
     */
    protected int offset;

    /**
     * @param reader
     */
    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.offset = reader.readInt16();
    }
}
