package org.jvm.instruction.base;

import org.jvm.rtda.thread.Frame;

/**
 * 简单分支跳转指令，包含if、goto
 *
 * @author 海燕
 * @date 2023/1/19
 */
public abstract class BRANCH extends BranchInstruction {

    /**
     * 获取跳转标识，根据标识执行跳转
     *
     * @param frame
     */
    @Override
    public void execute(Frame frame) {
        boolean cond = getCond(frame);
        if (cond) {
            branch(frame, this.offset);
        }
    }

    /**
     * 执行分支跳转
     *
     * @param frame
     * @param offset
     */
    protected void branch(Frame frame, int offset) {
        InstructionUtil.branch(frame, offset);
    }

    /**
     * 不同的指令有不同的判断规则
     *
     * @return
     */
    protected abstract boolean getCond(Frame frame);
}