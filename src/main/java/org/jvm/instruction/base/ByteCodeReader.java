package org.jvm.instruction.base;

import org.jvm.util.ByteArrayReader;

/**
 * author : 海燕
 * date : 2023/1/14
 * desc :读取字节码
 */
public class ByteCodeReader extends ByteArrayReader {

    public ByteCodeReader() {
        super(null);
    }


    public void reset(byte[] byteCode, int pc) {
        this.data = byteCode;
        this.count = pc;
    }

    /**
     * 获取程序计数器值
     *
     * @return
     */
    public int getPC() {
        return this.count;
    }

    /**
     * 部分场景需要程序计数器对其4的倍数
     */
    public void skipPadding() {
        for (; this.count % 4 != 0; count++) ;
    }
}