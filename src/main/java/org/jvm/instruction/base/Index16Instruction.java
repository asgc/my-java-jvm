package org.jvm.instruction.base;

/**
 * 用以表示双字节操作数的指令
 *
 * @author 海燕
 * @date 2023/1/14
 */
public abstract class Index16Instruction implements Instruction {

    protected int index;

    /**
     * @param reader
     */
    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.index = reader.readUint16();
    }
}
