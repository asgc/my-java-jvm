package org.jvm.instruction.base;

/**
 * 用以表示单字节操作数的指令
 *
 * @author 海燕
 * @date 2023/1/14
 */
public abstract class Index8Instruction implements Instruction {

    protected int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @param reader
     */
    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.index = reader.readUint8();
    }
}
