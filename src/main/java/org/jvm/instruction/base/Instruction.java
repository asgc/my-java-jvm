package org.jvm.instruction.base;

import org.jvm.rtda.thread.Frame;

/**
 * jvm指令
 *
 * @author 海燕
 * @date 2023/1/14
 */
public interface Instruction {

    /**
     * 从字节码中读取操作数
     */
    void fetchOperands(ByteCodeReader reader);

    /**
     * 再栈帧中执行指令
     *
     * @param frame
     */
    void execute(Frame frame);

}
