package org.jvm.instruction.comparisons.fcmp;

import org.jvm.rtda.thread.Frame;

/**
 * 比较指令
 *
 * @author 海燕
 * @date 2023/1/19
 */
public class FCMPL extends FCMP {

    @Override
    public void execute(Frame frame) {
        fcmpWithFlag(frame, false);
    }
}