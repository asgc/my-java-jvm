package org.jvm.instruction.comparisons.fcmp;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.*;

/**
 * @author 海燕
 * @date 2023/1/19
 */
public abstract class FCMP extends NoOperandsInstruction {

    public void fcmpWithFlag(Frame frame, boolean gFlag) {
        OperandStack stack = frame.getOperandStack();
        float v2 = stack.popFloat();
        float v1 = stack.popFloat();

        if (v1 > v2) {
            stack.pushInt(1);
        } else if (v1 == v2) {
            stack.pushInt(0);
        } else if (v1 < v2) {
            stack.pushInt(-1);
        } else if (gFlag) {
            stack.pushInt(1);
        } else {
            stack.pushInt(-1);
        }
    }
}