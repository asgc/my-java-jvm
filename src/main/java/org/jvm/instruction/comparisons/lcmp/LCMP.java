package org.jvm.instruction.comparisons.lcmp;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 比较指令
 *
 * @author 海燕
 * @date 2023/1/19
 */
public class LCMP extends NoOperandsInstruction {

    @Override
    public void execute(Frame frame) {
        long v2 = frame.getOperandStack().popLong();
        long v1 = frame.getOperandStack().popLong();

        if (v1 > v2) {
            frame.getOperandStack().pushInt(1);
        } else if (v1 == v2) {
            frame.getOperandStack().pushInt(0);
        } else {
            frame.getOperandStack().pushInt(-1);
        }
    }
}