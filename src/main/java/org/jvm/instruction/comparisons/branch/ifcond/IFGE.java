package org.jvm.instruction.comparisons.branch.ifcond;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public class IFGE extends IFCOND {
    @Override
    protected boolean compareIntToZero(int val) {
        return val >= 0;
    }
}
