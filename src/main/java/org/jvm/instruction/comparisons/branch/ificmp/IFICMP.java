package org.jvm.instruction.comparisons.branch.ificmp;

import org.jvm.instruction.base.BRANCH;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public abstract class IFICMP extends BRANCH {

    /**
     * 弹出栈顶的2个int，判断2个int的大小
     *
     * @return
     */
    @Override
    protected boolean getCond(Frame frame) {
        int val2 = frame.getOperandStack().popInt();
        int val1 = frame.getOperandStack().popInt();
        return compareInt(val1, val2);
    }

    protected abstract boolean compareInt(int val1, int val2);

}
