package org.jvm.instruction.comparisons.branch.ifcond;

import org.jvm.instruction.base.BRANCH;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public abstract class IFCOND extends BRANCH {

    /**
     * 弹出栈顶的一个int，判断int与0的比值
     *
     * @return
     */
    @Override
    protected boolean getCond(Frame frame) {
        int val = frame.getOperandStack().popInt();
        return compareIntToZero(val);
    }

    protected abstract boolean compareIntToZero(int val);
}
