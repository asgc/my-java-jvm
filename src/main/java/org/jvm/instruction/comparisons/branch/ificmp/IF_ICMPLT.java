package org.jvm.instruction.comparisons.branch.ificmp;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public class IF_ICMPLT extends IFICMP {
    @Override
    protected boolean compareInt(int val1, int val2) {
        return val1 < val2;
    }
}
